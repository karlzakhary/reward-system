class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :email
      t.float :score
      t.boolean :recommended

      t.timestamps
    end
  end
end
