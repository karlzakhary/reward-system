class User < ApplicationRecord
  validates :email, uniqueness: true
  attribute :score, :float, default: 0.0
  attribute :recommended, :boolean, default: false
  has_ancestry
end
