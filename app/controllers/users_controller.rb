class UsersController < ApplicationController
  before_action :set_user, only: [:show, :update, :destroy]
  skip_before_action :verify_authenticity_token
  require 'sendgrid-ruby'
  require 'csv'
  include SendGrid
  # GET /users
  def index
    @users = User.all
  end

  # GET /users/1
  def show
    render json: @user
  end

  # POST /users
  def create
    begin
      email = params.require(:email)

    rescue ActionController::ParameterMissing
      raise ActionController::BadRequest
    else
      if params[:recommender_id]
        begin
          inviter = User.find(params[:recommender_id])

          begin
            invitation = Invitation.find_by_invitee(email)
          rescue
            raise ActiveRecord::RecordNotFound
          else
            if invitation.nil?
              raise ActionController::BadRequest.new, "You are not invited"
            else
              unless invitation.inviter == inviter.email
                raise ActionController::BadRequest.new, "Wrong inviter"
              end
            end
            @child = User.create(email:email, recommended: true, parent: inviter )
          end
        rescue ActiveRecord::RecordNotFound
          raise ActiveRecord::RecordNotFound
        end
      else
        @child = User.new(user_params)
      end
      if @child.save
        update_score_helper(@child.parent, 0)
        render json: @child, status: :created, location: @child
      else
        render json: @child.errors, status: :unprocessable_entity
      end
    end
  end

  def recommend
    begin
      user_id = params.require(:id)
    rescue ActionController::ParameterMissing
      raise ActionController::BadRequest
    else
      begin
        @user = User.find(user_id)
      rescue ActiveRecord::RecordNotFound
        raise ActionController::BadRequest
      else
        begin
          recommended = params.require(:email)
          old_invitation = Invitation.find_by_invitee(recommended)
          unless old_invitation.nil?
            raise ActionController::BadRequest.new, "This user has been invited before!"
          end
        rescue ActionController::ParameterMissing
          raise ActionController::BadRequest
        else
          if User.find_by_email(recommended).nil?
            from = Email.new(email: 'RewardSystem@clark.de')
            to = Email.new(email: recommended)
            subject = 'Invitation is pending'
            val = 'User with email ' + @user.email + '. Has invited you over'+ "\n" + 'Press the next link to register ' + 'http://0.0.0.0:3000/users/?recommender_id='+ user_id
            content = Content.new(type: 'text/plain', value: val)
            mail = Mail.new(from, subject, to, content)
            sg = SendGrid::API.new(api_key: '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')
            response = sg.client.mail._('send').post(request_body: mail.to_json)
            invitation = Invitation.create(inviter: @user.email, invitee: recommended)
            render json: {"message":response}, status: :accepted
          else
            raise ActiveRecord::RecordNotUnique
          end

        end
      end
    end
  end


  def update_score_helper(user, pos)
    return if user.nil?
    @user = user
    score = @user.score + (0.5**pos)
    @user.update(score: score)
    update_score_helper(@user.parent, pos + 1)
  end

  def import
    @rowarray = Hash.new
    myfile = params[:file]

    @rowarraydisp = CSV.read(myfile.path, headers: false)
    for row in @rowarraydisp
      puts row
      @rowarray.store(User.find_by_email(row).email,User.find_by_email(row).score)
    end

    end

  # PATCH/PUT /users/1
  def update
    if @user.update(user_params)
      render json: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # DELETE /users/1
  def destroy
    @user.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.require(:user).permit(:email, :score, :recommended)
    end
end
