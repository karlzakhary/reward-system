# Clark Reward System

That application represents a system for calculating rewards based on recommendations of customers.



* Ruby version
    * ruby-2.3.7

* Steps to install project and dependencies:
    * `git clone https://gitlab.com/karlzakhary/reward-system/`
    * `cd reward-system`
    * `bundle install`

* Configuration
    * The server listens on port `3000`
    * The system uses SQLite
    
* Database initialization
    * `rails db:migrate`
    
* Running project
    * `rails s -e development`

* **API Endpoints**:
  * List Users `GET /users/`
  * Show User `GET /users/:id`
  * Create User `POST /users/`
    * Required Data Params `{"email":string}`
  * Recommend a user by an existing user and send invitation mail `POST /users/:id/recommend`
    * Required Url params `/:id/` of inviting User
    * Required Data Params `{"email":string}` of invited User
  * Create User by referral `POST /users/?recommender_id=int/`
    * Required Url params `:?recommender_id` of inviting User.
    * Required Data Params `{"email":string}` of invited User.
  * Custormers retrieval via importing customers emails `GET /users/` then import a .CSV file that contains the customers' emails whose scores are required, through the rendered view  in the format:  
     `root@gmail.com`  
     `child@gmail.com`  
     `grandchild@gmail.com`
