Rails.application.routes.draw do
  resources :invitations
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :users do
    member do
      post 'recommend'
      get 'update_score'
    end
    collection do
      post 'import'
    end
  end

end
